<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\loginAPI;
use App\Http\Controllers\UserController;
use App\Http\Controllers\banner;
use App\Http\Controllers\Homepage;
use Illuminate\Database\Seeders\UsersTableSeeder;
use App\Models\kari;

/*
|--------------------- -----------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'auth:sanctum'], function(){
    //All secure URL's
    
     });

Route::post("sign",[loginAPI::class,'signup']);
Route::post("login",[UserController::class,'index']);
Route::post("signup",[UsersTableSeeder::class,'run']);
Route::get("down",[Homepage::class,'down']);
Route::post("upload",[Homepage::class,'index']);
Route::get("itemdown",[Homepage::class,'idown']);
