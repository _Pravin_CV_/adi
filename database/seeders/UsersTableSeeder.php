<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Request $_req)
    {
        
       
            DB::table('users')->insert([
                'name' => $req->name,
                'email' => $req->email,
                'password' => Hash::make($req->password)
            ]
            );  
        
    }
}
