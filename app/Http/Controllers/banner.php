<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Models\Images;
use Image;


class banner extends Controller
{
    function index()
    {
        //$banner = Images::all();
        $banner = DB::table('images')->where('id','1')->get('banner_images');
        //mb_convert_encoding($banner['id'], 'UTF-8', 'UTF-8');
        print_r($banner);
        return $banner; 
    }

    function fetch_image()
    {
     $image = Images::all();

     $response = Response::make($image->encode('jpeg'));

     $response->header('Content-Type', 'image/jpeg');

     return $response;
    }
    
}
