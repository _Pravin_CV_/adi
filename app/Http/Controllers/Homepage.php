<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\item;
use App\Images;

class Homepage extends Controller
{
    //
    function index(Request $req)
    { 
        //print_r($req->file());
        $result=$req->file('banner')->store('upload');
        DB::table('images')->insert([
            'banner_name' => $req->name,
            'banner_image' => $result,
           ]
           );
           return $result;
    }

    function items(Request $req)
    { 
        $result=$req->file('item')->store('upload');
        DB::table('items')->insert([
            'item_name' => $req->name,
            'item_image' => $result,
           ]
           );
           return $result;
    }

    function down()
    {
        $list=Images::all();
        return $list;
    }

    function idown()
    {
        $item=item::all();
        $list=Images::all();
        return response()->json(['banner'=>$list,'items'=>$item]);
    }
}
